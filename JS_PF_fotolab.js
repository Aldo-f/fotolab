/* eslint-disable no-console */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* global var */
var aKleuren = [{
        naam: "kleur",
        prijs: 1.02,
        eenheid: "stuk"
    },
    {
        naam: "retro",
        prijs: 1.75,
        eenheid: "stuk"
    },
    {
        naam: "sepia",
        prijs: 1.20,
        eenheid: "stuk"
    },
    {
        naam: "zwart-wit",
        prijs: 0.95,
        eenheid: "stuk"
    },
]
var aFormaten = [
    ["A4", 1.95, "stuk"],
    ["A3", 3.55, "stuk"],
    ["A1", 5, "stuk"],
    ["10x15 cm", 0.89, "stuk"],
    ["20x30 cm", 1.29, "stuk"],
    ["10x15 cm", 9.20, "dozijn"],
    ["20x30 cm", 11.45, "dozijn"]
]
var aAfwerking = [{
        naam: "glans",
        prijs: 0.20
    },
    {
        naam: "mat",
        prijs: 0
    },
    {
        naam: "pearl",
        prijs: 1.25
    }
]
var aAankopen = []
var nTotaal = 0;

window.onload = function () {
    // alert("test");

    /* Zoek elementen
     *****************/
    var eKleur = document.getElementById("kleur");
    var eFormaat = document.getElementById("formaat");
    var eAfwerking = document.getElementById("afwerking");
    var eAantal = document.getElementById("aantal");
    var eToevoegen = document.getElementById("toevoegen");

    /* Plaats opties in select 
     ***************************/
    maakOpties(eKleur, aKleuren, "kleur");
    maakOpties(eFormaat, aFormaten, "formaat");
    maakOpties(eAfwerking, aAfwerking, "afwerking");

    /* Valideer 
     *************/
    eToevoegen.addEventListener('click', function (e) {
        e.preventDefault();
        if (valideer(eKleur, eFormaat, eAfwerking, eAantal)) {
            // Maak een aankooparray
            var aAankoop = voegToeAanAankoop(eKleur, eFormaat, eAfwerking, eAantal)
            // Toon de aankoop
            toonAankoop();
        }
    });

}

function maakOpties(e, array, s) {
    for (let i = 0; i < array.length; i++) {
        var eOptie = document.createElement("option");
        switch (s) {
            case "kleur":
                var sNaam = array[i].naam;
                var nPrijs = array[i].prijs;
                var sEenheid = array[i].eenheid;
                eOptie.value = sNaam;
                eOptie.innerHTML = sNaam + " (" + nPrijs + " €/" + sEenheid + ")";
                break;
            case "formaat":
                var sFormaat = array[i][0];
                var nPrijs = array[i][1];
                var sEenheid = array[i][2];
                eOptie.value = sFormaat + " (" + sEenheid + ")";
                eOptie.innerHTML = sFormaat + " (" + nPrijs + " €/" + sEenheid + ")";
                break;
            case "afwerking":
                var sNaam = array[i].naam;
                var nPrijs = array[i].prijs;
                eOptie.value = sNaam;
                eOptie.innerHTML = sNaam + " (" + nPrijs + " €/)";
                break;
            default:
                break;
        }
        e.appendChild(eOptie);
    }
}

function valideer(eKleur, eFormaat, eAfwerking, eAantal) {
    var sError = "";
    var bValideer = true;

    if (eKleur.value == "") {
        sError += "Kies een kleur\n";
        bValideer = false;
    }
    if (eFormaat.value == "") {
        sError += "Kies een formaat\n";
        bValideer = false;
    }
    if (eAfwerking.value == "") {
        sError += "Kies een afwerking\n"
        bValideer = false;
    }
    if (eAantal.value <= 0 || isNaN(eAantal.value)) {
        sError += "Er moet een geldig aantal, groter dan 0 in het vak staan"
        bValideer = false
    }

    if (sError != "") {
        alert(sError);
    }

    // console.log(bValideer);
    return bValideer;
}

function voegToeAanAankoop(eKleur, eFormaat, eAfwerking, eAantal) {
    var sFoto = document.querySelector('input[name="foto"]:checked').value;
    var sFormaat = eFormaat.value; //formaat + dozijn of stuk
    var bDozijn = sFormaat.includes("dozijn");
    var sFormaat = sFormaat.replace(/ *\([^)]*\) */g, ""); // Verwijderd tekst tussen ()
    var nAantal = eAantal.value;

    /* Pas foto aan indien nodig */
    sFoto = haalJuisteFotoOp(sFoto, eKleur.value)

    /* Controleer of zelfde rij reeds bestaat 
    en indien zo vermeerde je het aantal aanwezig met het nieuwe aantal */

    var bZelfde = false;
    if (aAankopen[0]) {
        var i = 0;
        while (!bZelfde && i < aAankopen.length) {
            bZelfde = bestaatReeds(i);
            i++;
        }
    }

    /* Plaats de aankoop in een array */
    if (bZelfde) {
        //Beide zijn hetzelfde, prijs werd eerder reeds berekend
        console.log('Toegevoegde bestaat reeds');
    } else {
        /* Bereken de prijs voor elke aankoop */
        var nPrijs = berekenDePrijs();

        var oAankoop;
        if (aAankopen[0]) {
            oAankoop = {
                foto: sFoto,
                kleur: eKleur.value,
                formaat: eFormaat.value,
                afwerking: eAfwerking.value,
                aantal: eAantal.value,
                prijs: nPrijs
            }
            aAankopen.push(oAankoop)

        } else {
            // Maak aan
            aAankopen = [{
                foto: sFoto,
                kleur: eKleur.value,
                formaat: eFormaat.value,
                afwerking: eAfwerking.value,
                aantal: eAantal.value,
                prijs: nPrijs
            }]
        }
    }

    function haalJuisteFotoOp(sFoto, sKleur) {
        if (sKleur != aKleuren[0].naam) {
            //Eerst .png er nog afhalen 
            sFoto = sFoto.substr(0, sFoto.lastIndexOf(".")) || sFoto;

            switch (sKleur) {
                case aKleuren[1].naam:
                    sFoto += "_retro"
                    break;
                case aKleuren[2].naam:
                    sFoto += "_sepia"
                    break;
                case aKleuren[3].naam:
                    sFoto += "_zwartwit"
                    break;
                default:
                    break;
            }
            //.png terug toevoegen
            sFoto += ".png"
        }
        return sFoto

    }

    function berekenDePrijs() {
        var sKleur = eKleur.value;
        var sAfwerking = eAfwerking.value;
        if (arguments.length) {
            nAantal = arguments[0];
        }
        /* Haal prijzen op */
        var nKleurprijs = prijsVan(sKleur, aKleuren);
        var nAfwerkingsprijs = prijsVan(sAfwerking, aAfwerking);
        var nFormaatprijs = prijsVanArray(sFormaat, aFormaten);

        console.log('Kleurenprijs: ' + nKleurprijs);

        /* Bereken */
        if (bDozijn) {
            nPrijs = ((nKleurprijs * nFormaatprijs) + (nAfwerkingsprijs * 12)) * nAantal
        } else {
            nPrijs = ((nKleurprijs * nFormaatprijs) + nAfwerkingsprijs) * nAantal
        }
        return Math.ceil(nPrijs * 100) / 100

        function prijsVan(s, array) {
            var i = 0;
            while (s != array[i].naam) {
                i++;
                if (i > array.length) {
                    // indien er een fout is
                    return;
                }
            }
            return array[i].prijs
        }

        function prijsVanArray(s, array) {
            var i = 0;
            while (s != array[i][0]) {
                i++;
                if (i > array.length) {
                    // indien er een fout is
                    return;
                }
            }
            console.log(i);

            if (bDozijn) {
                //
                i += 2
            }
            return array[i][1]
        }
    }

    function bestaatReeds(i) {
        if (aAankopen[i].foto == sFoto) {
            console.log('Zelfde foto en kleur');
            var sAankoopFormaat = aAankopen[i].formaat
            var bAankoopDozijn = sAankoopFormaat.includes("dozijn");
            if (bAankoopDozijn == bDozijn) {
                console.log('Zelfde eenheid');
                sAankoopFormaat = sAankoopFormaat.replace(/ *\([^)]*\) */g, "");
                if (sAankoopFormaat == sFormaat) {
                    console.log('Zelfde formaat');
                    if (aAankopen[i].afwerking == eAfwerking.value) {
                        console.log('Zelfde afwerking');

                        /* Tel aantallen op */
                        var nAankoopAantal = parseFloat(aAankopen[i].aantal);
                        nAankoopAantal += parseFloat(eAantal.value);
                        aAankopen[i].aantal = nAankoopAantal

                        /* Bereken totaal opnieuw */
                        var nPrijs = berekenDePrijs(nAankoopAantal);
                        aAankopen[i].prijs = nPrijs

                        return true;
                    }
                }
            }
        }
    }
}

function toonAankoop() {
    var eContainer = document.getElementById("containermand");

    // Verwijder inhoud
    while (eContainer.firstChild) {
        eContainer.removeChild(eContainer.firstChild);
    }

    /* Maak tabel */
    var eTabel = document.createElement("table");

    // Heading
    var eHead = document.createElement('thead');
    var eTheading = tableRow("th", "foto", "kleur", "formaat", "afwerking", "aantal", "totaal");
    eHead.appendChild(eTheading);

    // Inhoud (+ som totaal)
    nTotaal = 0;
    for (let i = 0; i < aAankopen.length; i++) {
        var eRow = tableRow("td", aAankopen[i].foto, aAankopen[i].kleur, aAankopen[i].formaat, aAankopen[i].afwerking, aAankopen[i].aantal, aAankopen[i].prijs);
        eHead.appendChild(eRow);
    }
    // Totaal noteren
    nTotaal = Math.ceil(nTotaal * 100) / 100
    console.log(nTotaal);
    var eTr = document.createElement("tr");
    var eTd = document.createElement("td");
    //row
    eTr.setAttribute("class", "totaalrij");
    eTr.appendChild(eTd);
    //lege td
    eTd.colSpan = 5;
    eTr.appendChild(eTd);
    //totaal
    eTd = eTd.cloneNode(true);
    eTd.innerHTML = nTotaal;
    eTd.colSpan = 1;
    eTd.setAttribute("class", "totaal");
    eTr.appendChild(eTd)
    //plaats tr
    eHead.appendChild(eTr);


    // Zet in elkaar
    eTabel.appendChild(eHead);
    eContainer.appendChild(eTabel);


    function tableRow(element, ...args) {
        var eTr = document.createElement("tr");

        for (var i = 0; i < args.length; i++) {
            if (i == 0 && args[i] != "foto") {
                // Maak een foto in de td
                var eFoto = document.createElement("img")
                eFoto.src = args[i];
                eFoto.alt = "foto van persoon";
                var e = document.createElement(element)
                e.appendChild(eFoto);
            } else {
                var e = document.createElement(element)
                e.innerHTML = args[i];
            }
            eTr.appendChild(e);
        }

        // Tel de totalen bij elkaar op
        if (!isNaN(args[5])) {
            nTotaal += parseFloat(args[5])
        }

        return eTr
    }
}